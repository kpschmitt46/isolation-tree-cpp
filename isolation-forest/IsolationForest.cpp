
#include <math.h>
#include <cassert>
#include "isolationForest.h"
#include "sort.h"

IsolationForest::IsolationForest(int n_estimators, double contamination)
{
	this->n_estimators = n_estimators;
	this->contamination = contamination;

	m = 0;
	threshold_ = NULL;
	estimators_ = new IsolationTree[n_estimators];
}

void IsolationForest::fit(DataFrame* dataFrame)
{
	int itree;
	for (itree = 0; itree < n_estimators; itree++)
		estimators_[itree].fit(dataFrame);

	m = dataFrame->ncols();

	// get threshold
	int nrows = dataFrame->nrows();
	double* scores = decision_function(dataFrame);

	//	set threshold for training set
	threshold_ = percentile(contamination, nrows, scores);
}

int* IsolationForest::predict(DataFrame* dataFrame)
{
	// initiation
	int nrows = dataFrame->nrows();
	int* is_inlier = new int[nrows];

	double* scores = decision_function(dataFrame);
	int irow;
	for (irow = 0; irow < nrows; irow++)
	{
		if (scores[irow] <= threshold_)
			is_inlier[irow] = -1;
		else
			is_inlier[irow] = 1;
	}

	return is_inlier;
}

double* IsolationForest::decision_function(DataFrame* dataFrame)
{
	// error checking
	assert(dataFrame->ncols() == m);

	// initiate predictions
	int nrows = dataFrame->nrows();
	double* scores = new double[nrows];

	// get heights for each point/tree
	int itree;
	int ** heights = new int *[n_estimators];
	for (itree = 0; itree < n_estimators; itree++)
		heights[itree] = estimators_[itree].predict(dataFrame);

	// get scores for each point
	int irow;
	int sum_heights;
	double average_height;
	for (irow = 0; irow < nrows; irow++)
	{
		// average heights
		sum_heights = 0;
		for (itree = 0; itree < n_estimators; itree++)
			sum_heights += heights[itree][irow];
		average_height = static_cast<double>(sum_heights) / static_cast<double>(n_estimators);

		// get score
		scores[irow] = anomaly_score(average_height, nrows);
	}

	return scores;

}

double IsolationForest::harmonic_number(int n)
{
	return log(static_cast<double>(n)) + 0.577215664901532;
}

double IsolationForest::average_path_length(int n)
{
	double double_n = static_cast<double>(n);
	return 2.0 * harmonic_number(n - 1) - 2.0 * (double_n - 1.0) / double_n;
}

double IsolationForest::anomaly_score(double path_length, int n)
{
	double c = average_path_length(n) - 1.0;
	double exp = -path_length / c;
	return 0.5 - pow(2.0, exp);
}