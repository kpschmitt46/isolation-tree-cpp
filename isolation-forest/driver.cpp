
#include <fstream>
#include <string>
#include <iostream>
#include <vector>
#include <math.h>

#include "IsolationForest.h"
#include "sort.h"


using namespace std;


int main()
{
	// Load data
	DataFrame df;
	df.load("iris.csv");

	// set output file
	

	//IsolationTree tree;
	//tree.fit(&df);
	//int height = tree.height();

	//int* levels;
	//levels = tree.predict(&df);

	//int irow;
	//for (irow = 0; irow < df.nrows(); irow++)
	//	cout << levels[irow] << endl;

	IsolationForest forest(100);
	forest.fit(&df);

	double* scores;
	scores = forest.decision_function(&df);

	// output to file
	ofstream outfile("iris.txt");
	int irow;
	for (irow = 0; irow < df.nrows(); irow++)
		outfile << scores[irow] << endl;
	outfile.close();

	//int n = 21;
	//double* kyle = new double[n];
	//int i;
	//for (i = 0; i < n; i++)
	//{
	//	kyle[i] = i * pow(-1, i);
	//	cout << kyle[i] << endl;
	//}

	//double x = percentile<double>(0.578, n, kyle);

	return 0;
}