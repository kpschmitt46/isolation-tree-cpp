

#include <stdlib.h>
#include <algorithm>
#include <time.h>
#include <cassert>
#include "IsolationTree.h"
#include <random>

using namespace std;

// Isolation tree node

IsolationTreeNode::IsolationTreeNode()
{
	splitCol = "";
	splitValue = NAN;
	index = nullptr;
	dataFrame = nullptr;
	lLink = nullptr;
	rLink = nullptr;
}

IsolationTreeNode::~IsolationTreeNode()
{

	delete lLink;
	delete rLink;
	lLink = nullptr;
	rLink = nullptr;

	delete[] index;
	index = nullptr;

	dataFrame = nullptr;

	splitCol = "";
	splitValue = NAN;
}

int IsolationTreeNode::nrows()
{
	return n;
}

void IsolationTreeNode::initiate(DataFrame* dataFrameInitial)
{

	// set dataframe
	dataFrame = dataFrameInitial;

	// set n
	n = dataFrame->nrows();

	// set index
	int irow;
	int * indexInitial = new int[n];
	for (irow = 0; irow < n; irow++)
		indexInitial[irow] = irow;
	index = indexInitial;
}

void IsolationTreeNode::split()
{

	// errors and exceptions
	assert (n > 1);

	// choose column and value to split on; repeat until finding column
	// with >1 unique value
	int irow, icol;
	int iattempt = 0;
	int ncol = dataFrame->ncols();
	double valmin, valmax, valcurr, f;
	do
	{
		iattempt++;

		// column to split on
		icol = rand() % ncol;
		splitCol = dataFrame->columns()[icol];

		// value to split on
		valmin = numeric_limits<double>::max();
		valmax = numeric_limits<double>::min();
		for (irow = 0; irow < n; irow++)
		{
			valcurr = dataFrame->operator[](splitCol)[index[irow]];
			if (valcurr < valmin)
				valmin = valcurr;
			if (valcurr > valmax)
				valmax = valcurr;
		}
		f = (double)rand() / (double)RAND_MAX;
		splitValue = f * (valmax - valmin) + valmin;

		// prevent infinite loop
		if (iattempt > 10 * ncol)
			int x = 1;

	} while (valmin == valmax);

	// split df
	int nLeft = 0;
	int nRight = 0;
	int * indexLeft = new int[n];
	int * indexRight = new int[n];
	for (irow = 0; irow < n; irow++)
	{
		valcurr = dataFrame->operator[](splitCol)[index[irow]];
		if (valcurr < splitValue)
			indexLeft[nLeft++] = index[irow];
		else
			indexRight[nRight++] = index[irow];
	}

	// create new nodes
	IsolationTreeNode* nodeLeft = new IsolationTreeNode;
	nodeLeft->n = nLeft;
	nodeLeft->index = indexLeft;
	nodeLeft->dataFrame = dataFrame;
	lLink = nodeLeft;

	IsolationTreeNode* nodeRight = new IsolationTreeNode;
	nodeRight->n = nRight;
	nodeRight->index = indexRight;
	nodeRight->dataFrame = dataFrame;
	rLink = nodeRight;

	// remove index
	delete[] index;
	index = nullptr;

}

void IsolationTreeNode::get_split_definition(std::string& splitCol, double& splitValue)
{
	splitCol = this->splitCol;
	splitValue = this->splitValue;
}

bool IsolationTreeNode::splittable() {

	if (n < 2)
		return false;

	int ncol = dataFrame->ncols();
	int irow, icol;
	for (icol = 0; icol < ncol; icol++)
	{
		splitCol = dataFrame->columns()[icol];
		for (irow = 0; irow < n - 1; irow++)
		{
			if (dataFrame->operator[](splitCol)[index[irow]] != dataFrame->operator[](splitCol)[index[irow + 1]])
				return true;
		}
	}
	return false;
}

// Isolation tree

IsolationTree::IsolationTree()
{

	root = nullptr;
	m = 0;

	// initialize random seed
	srand((unsigned int)time(NULL));
}

IsolationTree::~IsolationTree()
{
	m = 0;
	destroy(root);
}

void IsolationTree::fit(DataFrame* dataFrame)
{

	// save number of features of dataframe
	m = dataFrame->ncols();

	// initiate root node
	root = new IsolationTreeNode;
	root->initiate(dataFrame);
	
	// split
	recursive_split(root);
}

int* IsolationTree::predict(DataFrame* dataFrame)
{
	// error checking
	assert(dataFrame->ncols() == m);

	// initiate predictions
	int nrows = dataFrame->nrows();
	int* levels = new int[nrows];

	// get predictions
	int irow, level;
	string splitCol;
	double splitValue;
	IsolationTreeNode* currNode;
	for (irow = 0; irow < nrows; irow++) // for each row
	{
		currNode = root;
		level = 0;
		while (currNode->lLink != nullptr) // traverse tree
		{
			level++;
			currNode->get_split_definition(splitCol, splitValue);
			if (dataFrame->operator[](splitCol)[irow] < splitValue)
				currNode = currNode->lLink;
			else
				currNode = currNode->rLink;
		}
		levels[irow] = level;
	}
	return levels;

}

int IsolationTree::height() const
{
	return recursive_height(root);
}

int IsolationTree::recursive_height(IsolationTreeNode* node) const
{
	if (node == nullptr)
		return 0;
	else
		return 1 + max(recursive_height(node->lLink), recursive_height(node->rLink));
}

void IsolationTree::recursive_split(IsolationTreeNode* &node)
{
	if (node->splittable())
	{
		node->split();
		recursive_split(node->lLink);
		recursive_split(node->rLink);
	}
}

void IsolationTree::destroy(IsolationTreeNode * &node) const
{
	if (node != nullptr)
	{
		destroy(node->lLink);
		destroy(node->rLink);
		delete node;
		node = nullptr;
	}
}