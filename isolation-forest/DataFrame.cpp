
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

#include "DataFrame.h"

using namespace std;

DataFrame::DataFrame() {

	n = 0;
	m = 0;
	index = nullptr;
	data = nullptr;
}

DataFrame::~DataFrame() {
	destroy();
}

int DataFrame::nrows()
{
	return n;
}

int DataFrame::ncols()
{
	return m;
}

vector<string> DataFrame::columns()
{
	vector<string> v;
	for (map<string, int>::iterator it = cols.begin(); it != cols.end(); ++it) {
		v.push_back(it->first);
	}
	return v;
}

void DataFrame::destroy() {
	delete[] index;
	index = nullptr;

	int irow;
	for (irow = 0; irow < n; irow++)
		delete[] data[irow];
	delete[] data;
	data = nullptr;
}

vector<string> split(const string& str, const string& delim)
{
	vector<string> tokens;
	size_t prev = 0, pos = 0;
	do
	{
		pos = str.find(delim, prev);
		if (pos == string::npos) pos = str.length();
		string token = str.substr(prev, pos - prev);
		if (!token.empty()) tokens.push_back(token);
		prev = pos + delim.length();
	} while (pos < str.length() && prev < str.length());
	return tokens;
}

void DataFrame::load(string filename) {

	// reset
	n = 0;
	m = 0;
	cols.clear();
	destroy();

	// local variables
	string line;
	vector<string> parsed_line;

	// load file input stream
	ifstream dataFile(filename);

	// count rows
	while (getline(dataFile, line))
		++n;
	--n;
	dataFile.clear();
	dataFile.seekg(0, ios::beg);

	// parse headers
	getline(dataFile, line);
	parsed_line = split(line, ",");
	m = parsed_line.size();
	int icol = 0;
	vector<string>::iterator it;
	for (it = parsed_line.begin(); it != parsed_line.end(); it++) {
		cols[*it] = icol;
		icol++;
	}

	// set index
	int irow;
	index = new int[n];
	for (irow = 0; irow < n; irow++)
		index[irow] = irow;

	// allocate data
	data = new double *[n];
	for (irow = 0; irow < n; irow++)
		data[irow] = new double[m];

	// parse data
	irow = 0;
	while (getline(dataFile, line))
	{
		parsed_line = split(line, ",");
		icol = 0;
		vector<string>::iterator it;
		for (it = parsed_line.begin(); it != parsed_line.end(); it++) {
			data[irow][icol] = stod(*it);
			icol++;
		}
		irow++;
	}

}

double* DataFrame::operator[](string col) {

	// get column index
	int icol = cols[col];

	double* column = new double[n];
	int irow;
	for (irow = 0; irow < n; irow++)
		column[irow] = data[irow][icol];

	return column;
}