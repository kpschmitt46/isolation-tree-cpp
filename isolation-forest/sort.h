
template<class T>
inline void SWAP(T &a, T &b)
{
	T dum = a; a = b; b = dum;
}

template<class T>
T select(const int k, const int n, T *arr)
{

	int i, ir, j, l, mid;
	T a;
	l=0;
	ir=n-1;
	for (;;) {
		if (ir <= l+1) {
			if (ir == l+1 && arr[ir] < arr[l])
				SWAP(arr[l],arr[ir]);
			return arr[k];
		} else {
			mid=(l+ir) >> 1;
			SWAP(arr[mid],arr[l+1]);
			if (arr[l] > arr[ir])
				SWAP(arr[l],arr[ir]);
			if (arr[l+1] > arr[ir])
				SWAP(arr[l+1],arr[ir]);
			if (arr[l] > arr[l+1])
				SWAP(arr[l],arr[l+1]);
			i=l+1;
			j=ir;
			a=arr[l+1];
			for (;;) {
				do i++; while (arr[i] < a);
				do j--; while (arr[j] > a);
				if (j < i) break;
				SWAP(arr[i],arr[j]);
			}
			arr[l+1]=arr[j];
			arr[j]=a;
			if (j >= k) ir=j-1;
			if (j <= k) l=i;
		}
	}
}


template<class T>
T percentile(const double p, const int n, T *arr)
{
	double index_frac = static_cast<double>((n - 1) * p);
	int index = static_cast<int>(index_frac);
	double frac = index_frac - index;

	T lower = select(index, n, arr);
	T upper = select(index + 1, n, arr);
	return (1.0 - frac) * lower + frac * upper;

}