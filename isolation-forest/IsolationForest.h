
#include "IsolationTree.h"

class IsolationForest
{
public:

	IsolationForest(int n_estimators=100, double contamination=0.1);
	// constructor

	void fit(DataFrame*);
	// fit decision forest

	int* predict(DataFrame*);
	// predict category (+1 or -1) for each data point in dataframe

	double* decision_function(DataFrame*);
	// predict level for each data point in dataframe

private:

	int n_estimators;
	// number if trees in forest (set)

	double contamination;
	// percentile of anomaly threshold rel to training set (set)

	int m;
	// number of features (derived)

	double threshold_;
	// threshold for anomaly (derived)

	IsolationTree * estimators_;
	// array of IsolationTrees

	double harmonic_number(int);
	double average_path_length(int);
	double anomaly_score(double, int);
	// functions for scoring data point

};

