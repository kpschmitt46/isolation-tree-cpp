
#include <string>
#include <map>
#include <vector>

class DataFrame
{
public:

	DataFrame();
	// constructor

	~DataFrame();
	// destructor

	int nrows();
	// get number of rows

	int ncols();
	// get number of columns

	std::vector<std::string> columns();
	// get vector of column names

	void destroy();
	// destroy dataframe

	double* operator[](std::string);
	// indexing

	void load(std::string);
	// load from csv

private:

	// dimensions
	int n;
	int m;

	// index
	int * index;

	// column names
	std::map<std::string, int> cols;

	// data
	double ** data;

};

