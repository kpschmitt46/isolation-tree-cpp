
#include "DataFrame.h"

class IsolationTreeNode
{
public:
	IsolationTreeNode();
	// constructor

	~IsolationTreeNode();
	// destructor

	int nrows();
	// number of rows

	void initiate(DataFrame*);
	// associate dataframe to node

	void split();
	// randomly splits dataframe

	void get_split_definition(std::string&, double&);

	DataFrame* dataFrame;
	IsolationTreeNode *lLink;
	IsolationTreeNode *rLink;

	bool splittable();
	// checks if node can be split

private:
	
	int n;
	int * index;
	std::string splitCol;
	double splitValue;

};

class IsolationTree
{
public:
	
	IsolationTree();
	// constructor

	~IsolationTree();
	// destructor

	void fit(DataFrame*);
	// fit new decision tree

	int* predict(DataFrame*);
	// predict level for each data point in dataframe

	int height() const;
	// get height of tree

protected:
	IsolationTreeNode* root;

private:

	int m;
	// number of features

	void recursive_split(IsolationTreeNode* &node);
	// recursively split node containing dataframe

	int recursive_height(IsolationTreeNode* node) const;
	// recursively obtain height of subtree

	void destroy(IsolationTreeNode* &node) const;
	// destroy subtree attached to node p
};
